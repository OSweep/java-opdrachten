package dataStorage;

import java.util.ArrayList;
import domainModel.Loan;;

public interface LoanDAOInf {
	public ArrayList<Loan> getLoans();
}
