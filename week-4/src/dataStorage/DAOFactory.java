package dataStorage;

public class DAOFactory {
	private DAOFactory daoFactory;
	private MemberDAOInf memberDao;
	private LoanDAOInf loanDao;
	private ReservationDAOInf reservationDao;
	
	public DAOFactory getDAOFactory(){
		return daoFactory;
	}
	
	public MemberDAOInf getMemberDAO(){
		return memberDao;
	}
	
	public LoanDAOInf getLoanDAO(){
		return loanDao;
	}
	
	public ReservationDAOInf getReservationDAO(){
		return reservationDao;
	}
}