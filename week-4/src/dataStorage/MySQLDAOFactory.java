package dataStorage;

public class MySQLDAOFactory extends DAOFactory{
	private MemberDAOInf memberDao;
	private LoanDAOInf loanDao;
	private ReservationDAOInf reservationDao;
	
	@Override
	public MemberDAOInf getMemberDAO(){
		return memberDao;
	}
	
	@Override
	public LoanDAOInf getLoanDAO(){
		return loanDao;
	}
	
	@Override
	public ReservationDAOInf getReservationDAO(){
		return reservationDao;
	}
}
