package dataStorage;
import domainModel.Member;
import java.util.ArrayList;
public interface MemberDAOInf {
	
	public Member findMember();
	
	public void insertMember();
	
	public void updateMember();
	
	public void removeMember();
	
	public ArrayList<Member> listMembers();
	
}
