package dataStorage;
import domainModel.Member;
import java.util.ArrayList;

public class XMLDOMMemberDAO implements MemberDAOInf{
	private Member member;
	private ArrayList<Member> members;
	
	public Member findMember(){
		return member;
	}
	
	public void insertMember(){
		
	}
	
	public void updateMember(){
		
	}
	
	public void removeMember(){
		
	}
	
	public ArrayList<Member> listMembers(){
		return members;
	}
	
}