package counterthreads;

public class CounterStarter {
	public static void main(String[] args){
		Runnable counter = new Counter();
		
		Thread thread1 = new Thread(counter.run());
		Thread thread2 = new Thread(counter.decrement());
		Thread thread3 = new Thread(counter.value());
		
		thread1.start();
		thread2.start();
		thread3.start();
	}
}