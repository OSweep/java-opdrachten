package counterthreads;

public class Counter implements Runnable {
	private int c = 0;
	
	public void run(){
		increment();
		decrement();
		value();
	}
	
	public void increment(){
		c++;
	}
	
	public void decrement(){
		c--;
	}
	
	public int value(){
		return c;
	}
}
