package threads;

public class PrintChar implements Runnable {
	private char text;
	private int ammount;
	public PrintChar(char text, int ammount){
		this.text = text;
		this.ammount = ammount;
	}
	
	public void run(){
		for(int i = 0; i < ammount; i++){
			System.out.print(text);
		}
	}
}
