package threads;

public class PrintNum implements Runnable {
	private int ammount;
	public PrintNum(int ammount){
		this.ammount = ammount;
	}
	
	public void run(){
		for(int i = 0; i < ammount; i++){
			System.out.print(i);
		}
	}
}
