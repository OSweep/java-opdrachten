public class Hond extends Huisdier{
	public Hond(String naam){
		super(naam);
	}
	
	public void maakGeluid(){
		System.out.println( "Bark!");
		super.maakGeluid();
	}
	
	public void kwispel(){
		System.out.println("WIGGLE WIGGLE WIGGLE");
	}
}
