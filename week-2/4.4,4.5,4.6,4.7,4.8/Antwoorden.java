/*
a
Huisdier dier = new Hamster(); is niet toegestaan omdat hier eerst wordt 
gezegd dat variabele dier een object is van de klasse Huisdier. Vervolgens wordt aan deze 
variabele een nieuw Hamster object aangemaakt op de variabele die staat aangegeven als Huisdier

b 
Huisdier dier = new Kat( "Fritz" );
zelfde verhaal als bij a

c 
Huisdier dier = new Huisdier( "Pluk" ); is toegestaan als een van de constructors van de 
Huisdier klasse ��n String als parameter heeft

d 
Kat kat = new Huisdier( "Felix" );
zelfde verhaal als bij a

e 
Hamster hamster = new Hamster( "Knibbel" ); is toegestaan als een van de constructors van de 
Hamster klasse ��n String als parameter heeft
System.out.println(hamster.getNaam()); is toegestaan

f
Huisdier dier = new Hond( "Bobbie" );
zelfde verhaal als bij a
dier.maakGeluid(); is toegestaan

g
Huisdier dier = new Kat( "Droppie" );
zelfde verhaal als bij a
dier.spin(); is toegestaan

h
Kat kat = new Kat( "Zuus" ); is toegestaan als een van de constructors van de 
Kat klasse ��n String als parameter heeft
kat.spin(); is toegestaan

i
Hamster hamster = new Hond( "Gijs" );
hamster.kwispel(); is toegstaan omdat hamster een object van de klasse Hond is

j 
Huisdier dier = new Hond( "Tarzan" );
zelfde verhaal als bij a
((Hond) dier) .kwispel(); geen idee of dit is toegestaan
*/