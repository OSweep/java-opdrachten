import java.util.ArrayList;
public class Dierenwinkel {
	private ArrayList<Huisdier> lijst = new ArrayList<Huisdier>();
	public Dierenwinkel(){
	
	}
	
	public void voegToe(Huisdier dier){
		lijst.add(dier);
	}
	
	public void printOverzicht(){
		for(int i = 0; i < lijst.size(); i++) {
			System.out.println(lijst.get(i).getNaam());
		}
	}
}