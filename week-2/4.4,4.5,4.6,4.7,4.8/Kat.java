public class Kat extends Huisdier{
	public Kat(String naam){
		super(naam);
	}
	
	public void maakGeluid(){
		System.out.println("Miauw :3");
		super.maakGeluid();
	}
	
	public void spin(){
		System.out.println("Purr!");
	}
}
