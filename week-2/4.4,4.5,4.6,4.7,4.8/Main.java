
public class Main {

	public static void main(String[] args) {
		Dierenwinkel dw = new Dierenwinkel();
		Huisdier kat1 = new Kat("kat1");
		Huisdier kat2 = new Kat("kat2");
		Huisdier hond1 = new Hond("hond1");
		Huisdier hamster1 = new Hamster("hamster1");
		dw.voegToe(kat1);
		dw.voegToe(kat2);
		dw.voegToe(hond1);
		dw.voegToe(hamster1);
		dw.printOverzicht();
	}

}
