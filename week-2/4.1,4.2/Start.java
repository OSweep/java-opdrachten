import java.util.ArrayList;


public class Start {

	public static void main(String[] args) {
		Klant klant1 = new Klant("Klant 1", 1);
		//Klant klant2 = new Klant("Klant 2", 2);
		//Klant klant3 = new Klant("Klant 3", 3);
		Product product1 = new Product(1.11, "Product 1");
		Product product2 = new Product(2.22, "Product 2");
		Product product3 = new Product(3.33, "Product 3");
		ArrayList<Product> bestellijst1 = new ArrayList<Product>();
		bestellijst1.add(product1);
		bestellijst1.add(product2);
		bestellijst1.add(product3);
		Bestelling bestelling1 = new Bestelling(klant1, bestellijst1);
		bestelling1.Print();
	}

}