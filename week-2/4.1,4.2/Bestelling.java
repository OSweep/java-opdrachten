import java.util.ArrayList;
public class Bestelling {
	private ArrayList<Product> bestellijst = new ArrayList<Product>();
	private Klant klant;
	public Bestelling(Klant klant, ArrayList<Product> bestellijst){
		this.klant = klant;
		this.bestellijst = bestellijst;
	}
	
	public ArrayList<Product> getBestellijst(){
		return bestellijst;
	}
	
	public Klant getKlant(){
		return klant;
	}
	
	public void Print(){
		
		for(int i = 0; i < bestellijst.size(); i++) {
			Product product = bestellijst.get(i);
			System.out.println(product.toString());
		}
	}
}