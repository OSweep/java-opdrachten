public class Product {
	private String omschrijving;
	private double prijs;
	public Product(double prijs, String omschrijving){
		this.prijs = prijs;
		this.omschrijving = omschrijving;	
	}
	
	public double getPrijs() {
		return prijs;
	}
	
	public String toString() {
		String info = omschrijving + " �" + prijs;
		return info;
	}
}