import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;


public class txtWriter {
	public txtWriter(){
		
	}
	
	public void writeTxt(String src, String line) {
		PrintWriter uit = null;
		try {uit = new PrintWriter(new BufferedWriter(new FileWriter(src)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		uit.println(line);
		uit.close();
	}
	
	public void writePersonenTxt(String src, ArrayList<Persoon> personen) {
		String xml = "" +
		"<?xml version=\"1.0\"?>\n" +
		"<personen>\n";
		for(int i = 0; i < personen.size(); i++){
			Persoon persoon = personen.get(i);
			xml = xml +
		"	<persoon>\n" +
		"		<naam>" + persoon.getNaam() + "</naam>\n" +
		"		<adres>" + persoon.getAdres() + "</adres>\n" +
		"		<woonplaats>" + persoon.getWoonplaats() + "</woonplaats>\n" +
		"	</persoon>\n";
		}
		xml = xml +
		"</personen>";
		writeTxt(src, xml);
	}
}