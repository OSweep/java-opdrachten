import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class objectWriter{
	public objectWriter(){
		
	}
	
	public void writeObject(String src, Persoon persoon) {
		try {ObjectOutputStream uit = new ObjectOutputStream(new FileOutputStream(src));
			uit.writeObject(persoon);
			uit.close();
		} catch(IOException e) {
			System.out.println( "Fout bij wegschrijven persoon in bestand " + src);
			e.printStackTrace();
		}
	}
	
	public void writePersonen(String src, ArrayList<Persoon> personen) {
		try {ObjectOutputStream uit = new ObjectOutputStream(new FileOutputStream(src));
		uit.writeObject(personen);
		uit.close();
		} catch(IOException e) {
			System.out.println( "Fout bij wegschrijven personen in bestand " + src);
			e.printStackTrace();
		}		
	}
}