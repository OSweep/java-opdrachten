import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {
    	String src = "C:\\Users\\Jan Wintermans\\Documents\\AVANS\\23IVH5B3\\test.dat";
    	
    	Persoon persoon1 = new Persoon("Naam1", "Adres1", "Woonplaats1");
    	Persoon persoon2 = new Persoon("Naam2", "Adres2", "Woonplaats2");
    	Persoon persoon3 = new Persoon("Naam3", "Adres3", "Woonplaats3");
    	ArrayList<Persoon> personen = new ArrayList();
    	personen.add(persoon1);
    	personen.add(persoon2);
    	personen.add(persoon3);
    	
    	objectWriter ow = new objectWriter();
    	objectReader or = new objectReader();
    	txtWriter w = new txtWriter();
    	txtReader r = new txtReader();
    	
    	ow.writePersonen(src, personen);
    	or.readPersonen(src);
    	
    	ow.writeObject(src, persoon1);
    	or.readObject(src);
    	
    	w.writePersonenTxt(src, personen);
    	r.readPersonenTxt(src);
	}
}
/*
txtWriter w = new txtWriter();
w.writeTxt(src, "Deze tekst is met Java in een .txt bestand gezet.");
txtReader r= new txtReader();
r.readTxt(src);
*/